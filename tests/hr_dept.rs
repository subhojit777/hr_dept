extern crate hr_dept;

use hr_dept::*;

#[test]
fn test_hr_dept() {
    let mut department = Table::new();

    // Test correct input entries.
    if let Some(parsed_entry) = parser::parse_create_entry("Add Vegeta to Vegeta".to_string()) {
        crud::create(&mut department, parsed_entry);
    };

    if let Some(parsed_entry) = parser::parse_create_entry("Add Goku to Vegeta".to_string()) {
        crud::create(&mut department, parsed_entry);
    };

    if let Some(parsed_entry) = parser::parse_create_entry("Add Bardock to Vegeta".to_string()) {
        crud::create(&mut department, parsed_entry);
    };

    if let Some(parsed_entry) = parser::parse_create_entry("Add Muten Roshi to Earth".to_string()) {
        crud::create(&mut department, parsed_entry);
    };

    let vec = vec!["Vegeta".to_string(), "Goku".to_string(), "Bardock".to_string()];
    assert_eq!(crud::show_by_department(&department, "Vegeta"), Some(vec));

    let vec = vec!["Muten Roshi".to_string()];
    assert_eq!(crud::show_by_department(&department, "Earth"), Some(vec));

    // Test incorrect input entries.
    if let Some(parsed_entry) = parser::parse_create_entry("Spawn Kuririn in Earth".to_string()) {
        crud::create(&mut department, parsed_entry);
    };

    let vec = vec!["Muten Roshi".to_string(), "Kuririn".to_string()];
    assert_ne!(crud::show_by_department(&department, "Earth"), Some(vec));
    let vec = vec!["Muten Roshi".to_string()];
    assert_eq!(crud::show_by_department(&department, "Earth"), Some(vec));

    // Test sort.
    manipulate::sort(&mut department);

    let vec = vec!["Vegeta".to_string(), "Goku".to_string(), "Bardock".to_string()];
    assert_ne!(crud::show_by_department(&department, "Vegeta"), Some(vec));

    let vec = vec!["Bardock".to_string(), "Goku".to_string(), "Vegeta".to_string()];
    assert_eq!(crud::show_by_department(&department, "Vegeta"), Some(vec));

    let vec = vec!["Muten Roshi".to_string()];
    assert_eq!(crud::show_by_department(&department, "Earth"), Some(vec));

    // Test correct removal entries.
    if let Some(parsed_entry) = parser::parse_delete_entry("Remove Muten Roshi from Earth".to_string()) {
        crud::delete(&mut department, &parsed_entry);
    };

    let vec = Vec::new();
    assert_eq!(crud::show_by_department(&department, "Earth"), Some(vec));
    assert_ne!(crud::show_by_department(&department, "Earth"), None);

    // Test incorrect removal entries.
    if let Some(parsed_entry) = parser::parse_delete_entry("Send Goku to Earth".to_string()) {
        crud::delete(&mut department, &parsed_entry);
    };

    let vec = vec!["Bardock".to_string(), "Vegeta".to_string()];
    assert_ne!(crud::show_by_department(&department, "Vegeta"), Some(vec));

    let vec = vec!["Bardock".to_string(), "Goku".to_string(), "Vegeta".to_string()];
    assert_eq!(crud::show_by_department(&department, "Vegeta"), Some(vec));
}
