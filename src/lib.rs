use std::collections::HashMap;

pub mod parser;
pub mod manipulate;
pub mod crud;
pub type Table = HashMap<String, Vec<String>>;

/// Shows every person in every department.
pub fn show_all(database: &Table) {
    // TODO: At the moment I couldn't think of any way to write tests for this.
    // TODO: Because it literally prints everything to console, nothing else.
    // TODO: Find a way to write test for this function.

    // Print the entries in the database sorted by key.

    // MOTIVE: Create a Vec of HashMap keys, and sort the Vec, and print the
    // values in HashMap by key.

    // TODO: Find a better approach.
    // Problem: The size of the Vec can be changed during runtime.
    // Suggestion: The size of the Vec should be decided when it is created.
    let mut keys: Vec<String> = Vec::new();
    for key in database.keys() {
        keys.push(key.to_string());
    }

    keys.sort();

    for key in keys {
        if let Some(people) = database.get(&key) {
            if !people.is_empty() {
                println!("People who belong to {} department.", key);

                for person in people {
                    println!("{}", person);
                }
            }
        }
    }
}
