extern crate hr_dept;

use hr_dept::*;
use std::io;

fn main() {
    let mut database = Table::new();

    loop {
        println!("1. Add entry to the database.");
        println!("2. Remove entry from the database.");
        println!("3. Show all entries.");
        println!("4. Show entries by department.");
        println!("5. Quit.");
        println!("Enter your choice:");

        let mut choice = String::new();

        io::stdin().read_line(&mut choice)
            .expect("Failed to read line");

        let choice: u32 = match choice.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Incorrect input.");
                continue;
            },
        };

        match choice {
            1 => {
                println!("Enter the entry to be added to the database.");

                let mut entry = String::new();

                io::stdin().read_line(&mut entry)
                    .expect("Failed to read line");

                match parser::parse_create_entry(entry.trim().to_string()) {
                    Some(parsed_entry) => {
                        crud::create(&mut database, parsed_entry);
                        println!("Successfully added the entry.");
                    },
                    None => {
                        println!("Invalid entry entered. Please try again.");
                    },
                };
            },
            2 => {
                println!("Enter the removal entry.");

                let mut entry = String::new();

                io::stdin().read_line(&mut entry)
                    .expect("Failed to read line");

                match parser::parse_delete_entry(entry.trim().to_string()) {
                    Some(parsed_entry) => {
                        let result = crud::delete(&mut database, &parsed_entry);

                        if result.0 {
                            println!("Successfully removed {} from {}!", parsed_entry.1, parsed_entry.0);
                        } else if result.1 {
                            println!("{} not found in department {}", parsed_entry.1, parsed_entry.0);
                        } else {
                            println!("Department {} not found.", parsed_entry.0);
                        }
                    },
                    None => println!("Invalid entry entered. Please try again."),
                };
            },
            3 => {
                manipulate::sort(&mut database);
                show_all(&database);
            },
            4 => {
                println!("Enter the name of department whose employees you want to see.");

                let mut department = String::new();

                io::stdin().read_line(&mut department)
                    .expect("Failed to read line");

                manipulate::sort(&mut database);

                match crud::show_by_department(&database, department.trim()) {
                    Some(people) => {
                        if !people.is_empty() {
                            for person in people {
                                println!("{}", person);
                            }
                        } else {
                            println!("There are no people in this department.");
                        }
                    },
                    None => println!("Department by this name is not present."),
                }
            },
            5 => break,
            _ => {
                println!("Incorrect input.");
                continue;
            },
        };
    }
}
