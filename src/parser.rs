/// Parses the user input for creating new entry.
/// If entry successfully parsed, this will return `Some(tuple)`, where:
/// tuple.0 -> Name of the department.
/// tuple.1 -> Name of the person.
pub fn parse_create_entry(entry: String) -> Option<(String, String)> {
    // TODO: I am sure this can be improved.
    // TODO: There should be single point of return from this function.
    if let Some(_) = entry.find("Add") {
        if let Some(_) = entry.find("to") {
            // TODO: Make input casing less aggressive.
            // TODO: It should also support "add" and "TO".
            let result: Vec<&str> = entry.split("Add").collect();
            let result: Vec<&str> = result[1].split("to").collect();

            return Some((result[1].trim().to_string(), result[0].trim().to_string()));
        } else {
            return None;
        }
    } else {
        return None;
    }
}

/// Parses the user input for deleting an entry.
/// If entry successfully parsed, this will return `Some(tuple)`, where:
/// tuple.0 -> Name of the department.
/// tuple.1 -> Name of the person.
pub fn parse_delete_entry(entry: String) -> Option<(String, String)> {
    let entry_lowercase = entry.to_lowercase();
    let remove_length = "remove".len();
    let from_length = "from".len();

    if let Some(_) = entry_lowercase.find("remove") {
        if let Some(pos_from) = entry_lowercase.find("from") {
            return Some((entry[(pos_from + from_length)..].trim().to_string(), entry[remove_length..pos_from].trim().to_string()));
        } else {
            return None;
        }
    } else {
        return None;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_create_entry() {
        assert_eq!(parse_create_entry("Add Goku to Vegeta".to_string()), Some(("Vegeta".to_string(), "Goku".to_string())));
        assert_eq!(parse_create_entry("Put Goku in Vegeta".to_string()), None);
    }

    #[test]
    fn test_parse_delete_entry() {
        assert_eq!(parse_delete_entry("Remove Goku from Vegeta".to_string()), Some(("Vegeta".to_string(), "Goku".to_string())));
        assert_eq!(parse_delete_entry("remove Goku FROM Vegeta".to_string()), Some(("Vegeta".to_string(), "Goku".to_string())));
        assert_eq!(parse_delete_entry("Remove Goku From Vegeta".to_string()), Some(("Vegeta".to_string(), "Goku".to_string())));
        // TODO: Fix this bug.
        // assert_eq!(parse_delete_entry("Goku to be removed from Vegeta".to_string()), None);
        assert_eq!(parse_delete_entry("Pop Goku from Vegeta".to_string()), None);
    }
}
