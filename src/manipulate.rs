use super::Table;

/// Sorts people by their name in every department.
pub fn sort(database: &mut Table) {
    // TODO: Find a better approach.
    // Problem: The size of the Vec can be changed during runtime.
    // Suggestion: The size of the Vec should be decided when it is created.
    let mut keys: Vec<String> = Vec::new();
    for key in database.keys() {
        keys.push(key.to_string());
    }

    for key in keys {
        if let Some(people) = database.get_mut(&key) {
            people.sort();
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sort() {
        let mut department = Table::new();

        department.entry("Vegeta".to_string())
            .or_insert_with(Vec::new)
            .push("Goku".to_string());
        department.entry("Vegeta".to_string())
            .or_insert_with(Vec::new)
            .push("Bardock".to_string());

        department.entry("Earth".to_string())
            .or_insert_with(Vec::new)
            .push("Yamucha".to_string());
        department.entry("Earth".to_string())
            .or_insert_with(Vec::new)
            .push("Gohan".to_string());
        department.entry("Earth".to_string())
            .or_insert_with(Vec::new)
            .push("Muten Roshi".to_string());

        sort(&mut department);

        let vec = vec!["Bardock".to_string(), "Goku".to_string()];
        assert_eq!(department.get("Vegeta"), Some(&vec));

        let vec = vec!["Gohan".to_string(), "Muten Roshi".to_string(), "Yamucha".to_string()];
        assert_eq!(department.get("Earth"), Some(&vec));

        let vec = vec!["Yamucha".to_string(), "Gohan".to_string(), "Muten Roshi".to_string()];
        assert_ne!(department.get("Earth"), Some(&vec));
    }
}
