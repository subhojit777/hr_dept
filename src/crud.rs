use super::Table;

/// Adds a new entry to the record.
/// It creates a new department if it does not exist.
/// Otherwise, pushes the person to the department.
///
/// The entry to be created should be a `tuple`, where:
/// tuple.0 -> Name of the department.
/// tuple.1 -> Name of the person.
pub fn create(database: &mut Table, parsed_entry: (String, String)) {
    database.entry(parsed_entry.0)
        .or_insert_with(Vec::new)
        .push(parsed_entry.1);
}

/// Shows every people by department name.
pub fn show_by_department(database: &Table, department: &str) -> Option<Vec<String>> {
    match database.get(department) {
        Some(people) => return Some(people.to_vec()),
        None => return None,
    };
}

/// Deletes an entry from the record.
///
/// The entry to be created should be a `tuple`, where:
/// tuple.0 -> Name of the department.
/// tuple.1 -> Name of the person.
///
/// Returns Tuple -> `(bool, bool, bool)`
/// tuple.0 -> `true` if the entry is deleted, otherwise false.
/// tuple.1 -> `true` if person name is not found in department, otherwise false.
/// tuple.2 -> `true` if department is not found, otherwise false.
pub fn delete(database: &mut Table, parsed_entry: &(String, String)) -> (bool, bool, bool) {
    match database.get_mut(&parsed_entry.0) {
        Some(people) => {
            match people.iter().position(|person| *person == parsed_entry.1) {
                Some(position) => {
                    people.remove(position);

                    return (true, false, false);
                },
                None => return(false, true, false),
            };
        },
        None => return(false, false, true),
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create() {
        let mut department = Table::new();

        create(&mut department, ("Vegeta".to_string(), "Goku".to_string()));
        let vec = vec!["Goku".to_string()];
        assert_eq!(department.get("Vegeta"), Some(&vec));

        create(&mut department, ("Vegeta".to_string(), "Bardock".to_string()));
        let vec = vec!["Goku".to_string(), "Bardock".to_string()];
        assert_eq!(department.get("Vegeta"), Some(&vec));

        create(&mut department, ("Earth".to_string(), "Kuririn".to_string()));
        let vec = vec!["Kuririn".to_string()];
        assert_eq!(department.get("Earth"), Some(&vec));
        let vec = vec!["Goku".to_string(), "Bardock".to_string()];
        assert_eq!(department.get("Vegeta"), Some(&vec));
    }

    #[test]
    fn test_show_by_department() {
        let mut department = Table::new();

        department.entry("Vegeta".to_string())
            .or_insert_with(Vec::new)
            .push("Goku".to_string());
        department.entry("Vegeta".to_string())
            .or_insert_with(Vec::new)
            .push("Bardock".to_string());

        assert_eq!(show_by_department(&department, "Vegeta"), Some(vec!["Goku".to_string(), "Bardock".to_string()]));
    }

    #[test]
    fn test_delete() {
        let mut department = Table::new();

        department.entry("Vegeta".to_string())
            .or_insert_with(Vec::new)
            .push("Goku".to_string());
        department.entry("Vegeta".to_string())
            .or_insert_with(Vec::new)
            .push("Bardock".to_string());

        let result = delete(&mut department, &("Vegeta".to_string(), "Goku".to_string()));
        assert_eq!(result, (true, false, false));

        let result = delete(&mut department, &("Vegeta".to_string(), "Frieza".to_string()));
        assert_eq!(result, (false, true, false));

        let result = delete(&mut department, &("Frieza".to_string(), "Kuririn".to_string()));
        assert_eq!(result, (false, false, true));
    }
}
